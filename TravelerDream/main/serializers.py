from .models import *
from rest_framework import serializers
from django.contrib.auth.models import User
from django.contrib.auth.hashers import make_password
from rest_framework import pagination
from rest_framework.response import Response


class CustomPagination(pagination.PageNumberPagination):
    def get_paginated_response(self, data):
        return Response({
            'next': self.get_next_link(),
            'previous': self.get_previous_link(),
            'count': self.page.paginator.count,
            'total_pages': self.page.paginator.num_pages,
            'results': data,
            'page': self.page.number,
            'limit': self.page_size
        })


class EmployeeSerializer(serializers.ModelSerializer):
    imageUrl = serializers.ImageField(required=False)
    birthday = serializers.DateField(required=False)
    JobPosition = serializers.SlugRelatedField(slug_field="title", queryset=JobPosition.objects.all(), required=False)
    Organization = serializers.SlugRelatedField(slug_field="name", queryset=Organization.objects.all(), required=False)

    class Meta:
        model = Employee
        fields = ['JobPosition', 'Organization', 'imageUrl', 'birthday']


class UserBaseSerializer(serializers.ModelSerializer):
    employee = EmployeeSerializer(required=False)
    first_name = serializers.CharField(required=False)
    last_name = serializers.CharField(required=False)
    password = serializers.CharField(
        write_only=True,
        required=True,
        help_text='Leave empty if no change needed',
        style={'input_type': 'password', 'placeholder': 'Password'}
    )

    class Meta:
        model = User
        fields = ['id', 'username', 'first_name', 'last_name', 'email', 'password', 'employee']

    def create(self, validated_data):
        user = User.objects.create(
            username=validated_data['username'],
            email=validated_data['email'],
            password=make_password(validated_data['password']),
        )

        employee_data = dict(validated_data['employee'])

        image_url = None
        birthday = None

        if 'image_url' in employee_data:
            image_url = employee_data['imageUrl']

        if 'birthday' in employee_data:
            birthday = employee_data['birthday']

        Employee.objects.create(
            user=user,
            JobPosition=employee_data['JobPosition'],
            Organization=employee_data['Organization'],
            imageUrl=image_url,
            birthday=birthday
        )

        return user


class UserUpdateSerializer(UserBaseSerializer):
    password = serializers.CharField(
        write_only=True,
        required=False,
        help_text='Leave empty if no change needed',
        style={'input_type': 'password', 'placeholder': 'Password'}
    )

    class Meta:
        model = User
        fields = ['id', 'username', 'first_name', 'last_name', 'email', 'password', 'employee']

    def update(self, instance, validated_data):
        user = User.objects.get(id=instance.get('id'))
        user.username = instance.get('username')
        user.email = instance.get('email')
        if instance.get('first_name') is not None:
            user.first_name = instance.get('first_name')

        if instance.get('last_name') is not None:
            user.last_name = instance.get('last_name')
        user.save()

        job_position = JobPosition.objects.get(title=instance.get('JobPosition')) or None
        organization = Organization.objects.get(name=instance.get('Organization')) or None

        if job_position is not None or organization is not None:
            employee = user.employee
            employee.JobPosition = job_position
            employee.Organization = organization

            if instance.get('imageUrl'):
                employee.imageUrl = instance.get('imageUrl')

            if instance.get('birthday'):
                employee.birthday = instance.get('birthday')

            employee.save()

        return user


class ClientSerializer(serializers.ModelSerializer):
    ClientStatus = serializers.SlugRelatedField(slug_field='title', queryset=ClientStatus.objects.all())
    Passport = serializers.SlugRelatedField(slug_field='number', queryset=Passport.objects.all())

    class Meta:
        model = Client
        fields = '__all__'


class JobPositionSerializer(serializers.ModelSerializer):
    class Meta:
        model = JobPosition
        fields = '__all__'


class AgreementSerializer(serializers.ModelSerializer):
    Organization = serializers.SlugRelatedField(slug_field='name', queryset=Organization.objects.all())

    class Meta:
        model = Agreement
        fields = '__all__'


class CitySerializer(serializers.ModelSerializer):
    Country = serializers.SlugRelatedField(slug_field='name', queryset=Country.objects.all())

    class Meta:
        model = City
        fields = '__all__'


class ClientStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = ClientStatus
        fields = '__all__'


class ContractSerializer(serializers.ModelSerializer):
    Organization = serializers.SlugRelatedField(slug_field='name', queryset=Organization.objects.all())
    Agreement = serializers.SlugRelatedField(slug_field='number', queryset=Agreement.objects.all())

    class Meta:
        model = Contract
        fields = '__all__'


class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = '__all__'


class HotelSerializer(serializers.ModelSerializer):
    City = serializers.SlugRelatedField(slug_field='name', queryset=City.objects.all())

    class Meta:
        model = Hotel
        fields = '__all__'


class OrganizationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Organization
        fields = '__all__'


class PassportSerializer(serializers.ModelSerializer):
    class Meta:
        model = Passport
        fields = '__all__'


class PaymentSerializer(serializers.ModelSerializer):
    Organization = serializers.SlugRelatedField(slug_field='name', queryset=Organization.objects.all())
    Contract = serializers.SlugRelatedField(slug_field='number', queryset=Contract.objects.all())

    class Meta:
        model = Payment
        fields = '__all__'


class TourSerializer(serializers.ModelSerializer):
    City = serializers.SlugRelatedField(slug_field='name', queryset=City.objects.all())
    Agreement = serializers.SlugRelatedField(slug_field='number', queryset=Agreement.objects.all())
    Hotel = serializers.SlugRelatedField(slug_field='name', queryset=Hotel.objects.all())

    class Meta:
        model = Tour
        fields = '__all__'


class VoucherSerializer(serializers.ModelSerializer):
    class Meta:
        model = Voucher
        fields = '__all__'
