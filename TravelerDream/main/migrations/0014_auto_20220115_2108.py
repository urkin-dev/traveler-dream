# Generated by Django 3.1.4 on 2022-01-15 18:08

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0013_tour_hotel'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tour',
            name='Hotel',
            field=models.ForeignKey(db_column='Hotel', default=1, on_delete=django.db.models.deletion.DO_NOTHING, to='main.hotel'),
            preserve_default=False,
        ),
    ]
