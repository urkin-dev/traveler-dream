from django.urls import include, path
from rest_framework.routers import DefaultRouter
from .views import *

router = DefaultRouter()
router.register(r'vouchers', VoucherViewSet)
router.register(r'tours', TourViewSet)
router.register(r'payments', PaymentViewSet)
router.register(r'passports', PassportViewSet)
router.register(r'organizations', OrganizationViewSet)
router.register(r'hotels', HotelViewSet)
router.register(r'countries', CountryViewSet)
router.register(r'contracts', ContractViewSet)
router.register(r'client_statuses', ClientStatusViewSet)
router.register(r'cities', CityViewSet)
router.register(r'agreements', AgreementViewSet)
router.register(r'positions', JobPositionViewSet)
router.register(r'clients', ClientViewSet)
router.register(r'agents', AgentsViewSet)
router.register(r'unpaid', UnpaidPaymentsViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('login', login, name="login"),
    path('logout', logout, name="logout"),
    path('users/', UserView.as_view()),
    path('users/<int:id>', get_user),
    path('refresh-token', refresh_token, name='refresh-token'),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
