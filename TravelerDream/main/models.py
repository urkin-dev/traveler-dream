from django.conf import settings
from django.db import models


class JobPosition(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.title


class Employee(models.Model):
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    JobPosition = models.ForeignKey(
        JobPosition, models.CASCADE, db_column='JobPosition')
    Organization = models.ForeignKey(
        'Organization', models.DO_NOTHING, db_column='Organization')

    imageUrl = models.ImageField(
        upload_to='img/users', db_column='imageUrl', max_length=255, blank=True, null=True)
    birthday = models.DateField(blank=True, null=True)

    def __str__(self):
        return self.user.username


class Agreement(models.Model):
    Organization = models.ForeignKey(
        'Organization', models.DO_NOTHING, db_column='Organization')

    number = models.IntegerField(unique=True, max_length=16)
    date = models.DateField()

    def __str__(self):
        return str(self.number)


class City(models.Model):
    Country = models.ForeignKey(
        'Country', models.DO_NOTHING, db_column='Country')

    name = models.CharField(max_length=255)
    description = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.name


class Client(models.Model):
    ClientStatus = models.ForeignKey(
        'ClientStatus', models.DO_NOTHING, db_column='ClientStatus')
    Passport = models.ForeignKey(
        'Passport', models.CASCADE, db_column='Passport')

    name = models.CharField(max_length=255, blank=True, null=True)
    gender = models.CharField(max_length=32, blank=True, null=True)
    birthday = models.DateField(blank=True, null=True)
    birthplace = models.CharField(max_length=255, blank=True, null=True)

    Tour = models.ForeignKey(
        'Tour', models.DO_NOTHING, db_column='Tour', blank=True, null=True)

    def __str__(self):
        return self.name


class ClientStatus(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField(blank=True, null=True)

    class Meta:
        verbose_name = "ClientStatus"
        verbose_name_plural = "ClientStatuses"

    def __str__(self):
        return self.title


class Contract(models.Model):
    Organization = models.ForeignKey(
        'Organization', models.DO_NOTHING, db_column='Organization')
    Agreement = models.ForeignKey(
        Agreement, models.DO_NOTHING, db_column='Agreement')
    Currency = models.CharField(max_length=3, null=True)

    number = models.IntegerField(unique=True)
    date = models.DateField(blank=True, null=True)
    sum = models.FloatField()

    def __str__(self):
        return str(self.number)


class Country(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.name


class Hotel(models.Model):
    name = models.CharField(max_length=255)
    type = models.CharField(max_length=255, blank=True, null=True)
    address = models.CharField(max_length=255)
    checkInTime = models.DateField(
        db_column='checkInTime', blank=True, null=True)
    checkOutTime = models.DateField(
        db_column='checkOutTime', blank=True, null=True)

    City = models.ForeignKey(City, models.DO_NOTHING, db_column='City')

    def __str__(self):
        return self.name


class Organization(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.name


class Passport(models.Model):
    number = models.IntegerField(unique=True)
    fullName = models.CharField(db_column='fullName', max_length=255)
    registration = models.CharField(max_length=255)
    gender = models.CharField(max_length=32)
    birthday = models.DateField()
    issuedOn = models.DateField(db_column='issuedOn')
    issuedBy = models.CharField(db_column='issuedBy', max_length=255)

    def __str__(self):
        return str(self.number)


class Payment(models.Model):
    Organization = models.ForeignKey(
        Organization, models.DO_NOTHING, db_column='Organization')
    Contract = models.ForeignKey(
        Contract, models.DO_NOTHING, db_column='Contract')

    paid = models.BooleanField()
    number = models.IntegerField(unique=True)
    date = models.DateField()
    sum = models.FloatField()

    def __str__(self):
        return str(self.number)


class Tour(models.Model):
    City = models.ForeignKey(City, models.DO_NOTHING, db_column='City')
    Agreement = models.ForeignKey(
        Agreement, models.DO_NOTHING, db_column='Agreement')
    Hotel = models.ForeignKey(
        Hotel, models.DO_NOTHING, db_column='Hotel')

    ClientCount = models.IntegerField(db_column='ClientCount')
    startsAt = models.DateField(db_column='startsAt')
    endsAt = models.DateField(db_column='endsAt')

    def __str__(self):
        return str(self.City) + ' / ' + str(self.startsAt)


class Voucher(models.Model):
    Tour = models.ForeignKey(Tour, models.DO_NOTHING, db_column='Tour')

    transfer = models.BooleanField()
    travelDocuments = models.CharField(
        db_column='travelDocuments', max_length=255)

    def __str__(self):
        return self.travelDocuments
