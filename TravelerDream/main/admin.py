from django.contrib import admin
from .models import *


class JobPositionAdmin(admin.ModelAdmin):
    fields = ['title', 'description']


class ClientStatusAdmin(admin.ModelAdmin):
    fields = ['title', 'description']


class CountryAdmin(admin.ModelAdmin):
    fields = ['name', 'description']


class OrganizationAdmin(admin.ModelAdmin):
    fields = ['name', 'description']


class EmployeeAdmin(admin.ModelAdmin):
    fields = ['user', 'JobPosition', 'Organization', 'imageUrl', 'birthday']


class ClientAdmin(admin.ModelAdmin):
    fields = ['ClientStatus', 'Passport', 'name',
              'birthday', 'gender', 'birthplace', 'Tour']


class AgreementAdmin(admin.ModelAdmin):
    fields = ['Organization', 'number', 'date']


class ContractAdmin(admin.ModelAdmin):
    fields = ['Organization', 'Agreement', 'Currency', 'number', 'date', 'sum']


class PaymentAdmin(admin.ModelAdmin):
    fields = ['Organization', 'Contract', 'paid', 'number', 'date', 'sum']


class HotelAdmin(admin.ModelAdmin):
    fields = ['name', 'type', 'address', 'checkInTime', 'checkOutTime', 'City']


class PassportAdmin(admin.ModelAdmin):
    fields = ['number', 'fullName', 'registration',
              'gender', 'birthday', 'issuedOn', 'issuedBy']


class CityAdmin(admin.ModelAdmin):
    fields = ['Country', 'name', 'description']


class TourAdmin(admin.ModelAdmin):
    fields = ['City', 'Agreement', 'Hotel', 'ClientCount', 'startsAt', 'endsAt']


class VoucherAdmin(admin.ModelAdmin):
    fields = ['Tour', 'transfer', 'travelDocuments']


admin.site.register(Hotel, HotelAdmin)
admin.site.register(JobPosition, JobPositionAdmin)
admin.site.register(ClientStatus, ClientStatusAdmin)
admin.site.register(Country, CountryAdmin)
admin.site.register(Organization, OrganizationAdmin)
admin.site.register(Agreement, AgreementAdmin)
admin.site.register(Contract, ContractAdmin)
admin.site.register(Payment, PaymentAdmin)
admin.site.register(Passport, PassportAdmin)
admin.site.register(Employee, EmployeeAdmin)
admin.site.register(Client, ClientAdmin)
admin.site.register(City, CityAdmin)
admin.site.register(Tour, TourAdmin)
admin.site.register(Voucher, VoucherAdmin)
