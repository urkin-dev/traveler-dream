import jwt
from django.conf import settings
from django.contrib.auth.models import User
from rest_framework import exceptions, status, permissions
from rest_framework import viewsets
from rest_framework.decorators import api_view, permission_classes
from .utils import generate_access_token, generate_refresh_token
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from .models import Voucher, Tour, Payment, Passport, Organization, Hotel, Country, Contract, Client, City, \
    Agreement, JobPosition, ClientStatus, Employee
from .serializers import VoucherSerializer, TourSerializer, PaymentSerializer, PassportSerializer, \
    OrganizationSerializer, HotelSerializer, CountrySerializer, ContractSerializer, \
    ClientSerializer, CitySerializer, AgreementSerializer, JobPositionSerializer, ClientStatusSerializer, \
    UserBaseSerializer, UserUpdateSerializer


class UserView(APIView):
    @staticmethod
    def get(request):
        users = User.objects.all()
        serializer = UserBaseSerializer(users, many=True)
        return Response(serializer.data)

    @staticmethod
    def post(request):
        serializer = UserBaseSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        if serializer.errors:
            return Response(serializer.errors, status=status.HTTP_409_CONFLICT)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @staticmethod
    def put(request):
        user_id = request.data.get('id')
        user = User.objects.filter(id=user_id).first()

        if user is None:
            return Response("User doesn't exist", status.HTTP_404_NOT_FOUND)

        serializer = UserUpdateSerializer(instance=user, data=request.data)
        if serializer.is_valid():
            user = serializer.update(instance=request.data, validated_data=serializer.data)
            user = UserUpdateSerializer(user).data
            return Response(user)
        else:
            return Response(serializer.errors, status.HTTP_409_CONFLICT)

    @staticmethod
    def delete(request):
        user_id = request.data.get('id')
        password = request.data.get('password')
        user = User.objects.filter(id=user_id).first()

        if not user.check_password(password):
            return Response('Wrong password or username', status.HTTP_401_UNAUTHORIZED)
        else:
            user = user.delete()
            return Response(user, status=status.HTTP_200_OK)

    def get_permissions(self):
        if self.request.method == 'PUT':
            self.permission_classes = [IsAuthenticated]
        elif self.request.method == 'DELETE':
            self.permission_classes = [IsAuthenticated]
        else:
            self.permission_classes = []

        return super(UserView, self).get_permissions()


class AgentsViewSet(viewsets.ModelViewSet):
    position = JobPosition.objects.get(title="agent")
    employees = Employee.objects.filter(JobPosition=position)
    queryset = User.objects.filter(employee__in=employees)
    serializer_class = UserBaseSerializer


class UnpaidPaymentsViewSet(viewsets.ModelViewSet):
    queryset = Payment.objects.filter(paid=False)
    serializer_class = PaymentSerializer


class ClientViewSet(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class JobPositionViewSet(viewsets.ModelViewSet):
    queryset = JobPosition.objects.all()
    serializer_class = JobPositionSerializer


class AgreementViewSet(viewsets.ModelViewSet):
    queryset = Agreement.objects.all()
    serializer_class = AgreementSerializer


class CityViewSet(viewsets.ModelViewSet):
    queryset = City.objects.all()
    serializer_class = CitySerializer


class ClientStatusViewSet(viewsets.ModelViewSet):
    queryset = ClientStatus.objects.all()
    serializer_class = ClientStatusSerializer


class ContractViewSet(viewsets.ModelViewSet):
    queryset = Contract.objects.all()
    serializer_class = ContractSerializer


class CountryViewSet(viewsets.ModelViewSet):
    queryset = Country.objects.all()
    serializer_class = CountrySerializer


class HotelViewSet(viewsets.ModelViewSet):
    queryset = Hotel.objects.all()
    serializer_class = HotelSerializer


class OrganizationViewSet(viewsets.ModelViewSet):
    queryset = Organization.objects.all()
    serializer_class = OrganizationSerializer


class PassportViewSet(viewsets.ModelViewSet):
    queryset = Passport.objects.all()
    serializer_class = PassportSerializer


class PaymentViewSet(viewsets.ModelViewSet):
    queryset = Payment.objects.all()
    serializer_class = PaymentSerializer


class TourViewSet(viewsets.ModelViewSet):
    queryset = Tour.objects.all()
    serializer_class = TourSerializer


class VoucherViewSet(viewsets.ModelViewSet):
    queryset = Voucher.objects.all()
    serializer_class = VoucherSerializer


@api_view(['POST'])
@permission_classes([AllowAny])
def login(request):
    username = request.data.get('username')
    email = request.data.get('email')
    password = request.data.get('password')
    response = Response()

    if (username is None) or (password is None):
        return Response('Wrong password or username', status.HTTP_401_UNAUTHORIZED)

    user = User.objects.filter(username=username, email=email).first()

    if user is None or not user.check_password(password):
        return Response('Wrong password or username', status.HTTP_401_UNAUTHORIZED)

    serialized_user = UserBaseSerializer(user).data

    access_token = generate_access_token(user)
    refresh_token = generate_refresh_token(user)

    response.set_cookie(key='refreshtoken', value=refresh_token, httponly=True)

    response.data = {
        'accessToken': access_token,
        'user': serialized_user,
    }

    return response


@api_view(['POST'])
@permission_classes([AllowAny])
def logout(request):
    response = Response()
    response.delete_cookie('refreshtoken')

    return response


@api_view(['POST'])
@permission_classes([AllowAny])
def refresh_token(request):
    refresh_token = request.COOKIES.get('refreshtoken')
    if refresh_token is None:
        raise exceptions.AuthenticationFailed(
            'Authentication credentials were not provided.')
    try:
        payload = jwt.decode(
            refresh_token, settings.REFRESH_TOKEN_SECRET, algorithms=['HS256'])
    except jwt.ExpiredSignatureError:
        raise exceptions.AuthenticationFailed(
            'expired refresh token, please login again.')

    user = User.objects.filter(id=payload.get('user_id')).first()
    if user is None:
        raise exceptions.AuthenticationFailed('User not found')

    if not user.is_active:
        raise exceptions.AuthenticationFailed('user is inactive')

    access_token = generate_access_token(user)
    return Response({'accessToken': access_token})


@api_view(['GET'])
def get_user(request, id):
    response = Response()
    if id is None:
        raise exceptions.AuthenticationFailed('UserID is required')

    user = User.objects.get(id=id)

    if user is None:
        raise exceptions.AuthenticationFailed('User not found')

    serialized_user = UserBaseSerializer(user).data

    response.data = {
        'user': serialized_user,
    }

    return response
